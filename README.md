# udp2nsq

A tool that accepts UDP packets and sends it to a NSQd as a message on a given topic.

## To use

```shell
udp2nsq -listenport :514 -nsqd-tcp-address localhost:4150
```

## Sending JSON

If your input is text you can forward it as a JSON using ```-json```. The message will look something like:

```json
{
  "msg": "your message here",
  "hash": "hash of msg if you also used -hash",
  "src_addr": "host:port or [host]:port with IPv6 - port is shown by default, but can be removed by -port=disable",
  "received_time": unix_timestamp
}
```

## Crosscompile

To compile a static binary for Linux use:
```
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build .
```
