package main

import (
	"errors"
	"flag"
	"os"
)

var (
	topic           = flag.String("topic", "", "topic to publish to")
	udpListenPort   = flag.String("listenport", "", "UDP listen port as ip:port or :port")
	destNsqdTCPAddr = flag.String("nsqd-tcp-address", "", "destination nsqd TCP port")
	httpPort        = flag.Int("http", 0, "http listen port (metrics)")

	showHelp = flag.Bool("help", false, "show help")

	bufSize   = flag.Int("buffer", 2000, "buffer size for received messages")
	queueSize = flag.Int("queue", 100, "queue for in-flight messages")

	sendJson = flag.Bool("json", false, "send message as JSON message")
	showPort = flag.Bool("port", true, "if the port number will be added to JSON (with IP address")
	makeHash = flag.Bool("hash", false, "create hash for each message - valid with json")
)

const (
	envTopic      = "TOPIC"
	envListenPort = "LISTEN"
	envdestNSQ    = "NSQ"
)

// makeErr creates an error from text
func makeErr(text string) error {
	return errors.New(text)
}

// parseCmdLine parses all command line parameters and checking environment variables if not defined
func parseCmdLine() error {
	flag.Parse()
	if *showHelp {
		flag.PrintDefaults()
		os.Exit(0)
	}
	// topic
	if len(*topic) == 0 {
		*topic = os.Getenv(envTopic)
	}
	if len(*topic) == 0 {
		return makeErr("missing topic")
	}
	// UDP listen port
	if len(*udpListenPort) == 0 {
		*udpListenPort = os.Getenv(envListenPort)
	}
	if len(*udpListenPort) == 0 {
		return makeErr("missing listen port")
	}
	// NSQd
	if len(*destNsqdTCPAddr) == 0 {
		*destNsqdTCPAddr = os.Getenv(envdestNSQ)
	}
	if len(*destNsqdTCPAddr) == 0 {
		return makeErr("missing NSQd")
	}
	return nil
}
