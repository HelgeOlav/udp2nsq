package main

import (
	"bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/version"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/nsqio/go-nsq"
	"hash"
	"log"
	"net"
	"strings"
	"time"
)

// Msg is what is sent between the sender and receiver threads
type Msg struct {
	msg  []byte   // the data that we received
	addr net.Addr // the address the packet originated from
}

// mainloop is the workhorse, processing all messages
func mainloop() error {
	initMetrics()
	// transportChan passes messages from receiver to sender
	transportChan := make(chan Msg, *queueSize)
	// start receiver
	err := receiver(transportChan)
	if err != nil {
		return err
	}
	// start sender
	return senderNSQAPI(transportChan)
}

// TODO: rewrite to use SO_REUSEPORT and more workers to increase receive performance.
// receiver creates a receive thread, return error if it did not start.
func receiver(sendto chan Msg) error {
	pc, err := net.ListenPacket("udp", *udpListenPort)
	if err != nil {
		return err
	}
	go func() {
		defer pc.Close()
		for {
			buf := make([]byte, *bufSize)
			n, addr, err := pc.ReadFrom(buf)
			if n > 0 {
				m := Msg{
					msg:  buf[:n],
					addr: addr,
				}
				msgReceived.Inc()
				sendto <- m
			}
			if err != nil {
				log.Println(err)
				return
			}
		}
	}()
	return nil
}

// JsonMessage is the message sent out if it is a JSON message
type JsonMessage struct {
	Msg         string `json:"msg"`            // the message
	Hash        string `json:"hash,omitempty"` // the hash if configured
	SrcAddr     string `json:"src_addr"`       // source address and port of packet
	ReceiveTime int64  `json:"received_time"`  // time when message was received
}

// hasher how we hash

// MakeHash creates a hash of the message if len>0
func (j *JsonMessage) MakeHash() {
	if len(j.Msg) > 0 {
		var hasher hash.Hash = md5.New()
		hasher.Write([]byte(j.Msg))
		j.Hash = base64.RawURLEncoding.EncodeToString(hasher.Sum(nil))
	}
}

// senderNSQAPI is responsible for sending a message using the NSQ library
func senderNSQAPI(msg chan Msg) error {
	// set up NSQ
	cfg := nsq.NewConfig()
	ver := version.Get()
	cfg.UserAgent = fmt.Sprintf("%s go-nsq/%s", ver.SimpleVersion(), nsq.VERSION)
	producer, err := nsq.NewProducer(*destNsqdTCPAddr, cfg)
	if err != nil {
		return err
	}
	go func() {
		for {
			select {
			case m := <-msg:
				var err error
				if *sendJson {
					myMsg := JsonMessage{
						Msg:         string(m.msg),
						SrcAddr:     m.addr.String(),
						ReceiveTime: time.Now().Unix(),
					}
					if *makeHash {
						myMsg.MakeHash()
					}
					if !*showPort {
						pos := strings.LastIndex(myMsg.SrcAddr, ":")
						if pos > 0 {
							myMsg.SrcAddr = myMsg.SrcAddr[:pos]
						}
					}
					myJson, err := json.Marshal(&myMsg)
					if err == nil {
						err = producer.Publish(*topic, myJson)
					}
				} else {
					// send as plain text message
					err = producer.Publish(*topic, m.msg)
				}
				msgSent.Inc()
				// check for error
				if err != nil {
					msgSentError.Inc()
					log.Println(err)
				}
			case <-utils.CtrlCchannel():
				producer.Stop()
				return
			}
		}
	}()
	return nil
}
