package main

import (
	"bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/version"
	"log"
	"time"
)

func main() {
	ver := version.Get()
	log.Println(ver.String())
	err := parseCmdLine()
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("Receiving on %s, sending to topic %s on %s\n", *udpListenPort, *topic, *destNsqdTCPAddr)
	err = mainloop()
	if err != nil {
		log.Println(err)
		return
	}
	<-utils.CtrlCchannel()
	log.Println("exiting")
	time.Sleep(time.Second)
}
