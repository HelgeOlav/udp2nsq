package main

import "bitbucket.org/HelgeOlav/utils/version"

func init() {
	version.NAME = "udp2nsq"
	version.VERSION = "0.1.3"
}
