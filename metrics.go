package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"strconv"
)

var (
	msgReceived  prometheus.Counter // increased if we receive a message
	msgSent      prometheus.Counter // increased if we send a message
	msgSentError prometheus.Counter // increased if we got an error when sending a message
)

func initMetrics() {
	msgSentError = promauto.NewCounter(prometheus.CounterOpts{
		Name: "msg_sent_error",
		Help: "Number of messages sent with error",
	})
	msgSent = promauto.NewCounter(prometheus.CounterOpts{
		Name: "msg_sent",
		Help: "Number of messages sent",
	})
	msgReceived = promauto.NewCounter(prometheus.CounterOpts{
		Name: "msg_received",
		Help: "Number of messages received",
	})
	// start http server
	if *httpPort > 0 {
		listen := ":" + strconv.Itoa(*httpPort)
		http.Handle("/metrics", promhttp.Handler())
		log.Println("starting http server on", listen)
		go log.Println(http.ListenAndServe(listen, nil))
	}
}
